#include <iostream>
#include <ctime>
#include <array>
#include <iomanip>
#include <vector>

#include "Find2DPeak.h"

using namespace std;

int main()
{
	
	const int max = 25;
	bool again = true;
	srand(time(NULL));


	double matrix[N][N] = {0};

	while(again)
	{
		randomizeMatrix(matrix, max);
		
		printArray(matrix);

		int * peakxy = find2DPeak(matrix, N, 0, N);
		cout << "\n\n One Peak Located at (" << peakxy[0] << "," << peakxy[1] << ")" << endl << endl;

		if(matrix[peakxy[0]][peakxy[1]] >= matrix[peakxy[0]][peakxy[1]+1] 
		&& matrix[peakxy[0]][peakxy[1]] >= matrix[peakxy[0]][peakxy[1]-1]
		&& matrix[peakxy[0]][peakxy[1]] >= matrix[peakxy[0]+1][peakxy[1]]
		&& matrix[peakxy[0]][peakxy[1]] >= matrix[peakxy[0]-1][peakxy[1]])
			cout << "\n Peak Confirmed ! \n";
		else
			cout << "\n Fail! \n";

		system("pause");
		system("cls");
	}
	
	// clean up memory
	for(int i = 0; i < N; i++)
		delete [] matrix;

	system("pause");
return 0;
}


