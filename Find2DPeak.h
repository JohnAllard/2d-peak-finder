/**
*	@author John Allard
*	This file holds the find2DPeak function and it's helper functions. This function is
*	used to find a peak on in a 2D matrix.  A peak is defined as a coordinate in the matrix that is >=
*	all 2-4 directly adjascent coordinates. Note that this function only finds a peak and not the max.
*
*	Running time of algorithm is O(nlog(m))  where n is the number of rows and m is columns
*
*	**PROOF**
*	given matrix with size (n, m), this function will pick the middle column, and find the maximum
*	element in that column. It will then compare that element with ones in adjeascent columns, if either column
*	has a greater value than the current one, we will recurse on that half of the matrix (cut column-wise), else we  
*	know we have found a peak. It takes linear worst case time to find the maximum element in any one column,
*	and we will have to recurse at worse case log(m) times. 
*	Base case is where we have one column, then we simply find the maximum element in that column to find the peak.
* 
*	Recurrance 
*	T(n,m) = T(n, m/2) + O(n)
*	Takes log(m) times to reach base case, thus total running time is
*	T(n) = O(n) + O(n) + .... + O(n) log(m) times.
*	Thus T(n) = O(nlog(m))
*
*	**END PROOF**
*/

#include <iostream>
#include <ctime>
#include <iomanip>
#include <vector>

const int N = 10;
using namespace std;

int findGlobalMax(double arr[N][N], int rows, int column);
int * find2DPeak(double matrix[N][N], int rows, int left, int right);
void printArray(double matrix[N][N]);
void randomizeMatrix(double matrix[N][N], int max);

/**
*	@param matrix - This is the matrix that we will find a peak in. Can be an NxM matrix, is currently just square. 
*	@param rows - number of rows in the matrix
*	@param left - the index of the left column (we are searching column left through right)
*	@param right - the index of the right column (relative to the original matrix)
*
*	@return a 2d int array, the first value is the x (row) coordinate of the peak, second index is
*	the y (column) coordinate of the peak.
*/
int * find2DPeak(double matrix[N][N], int rows, int left, int right)
{
	static int coord[2] = {0,0};

	// base case
	if(right == left)
	{
		coord[0] = findGlobalMax(matrix, rows, left);
		coord[1] = left;

		return coord;
	}

	// new column is in the middle of the matrix;
	int newcolumn = (int)(right + left)/2;
	int maxrow = findGlobalMax(matrix, rows, newcolumn);

	
	if(newcolumn != N && matrix[maxrow][newcolumn] < matrix[maxrow][newcolumn+1])
		return find2DPeak(matrix, rows, newcolumn, right);
	else if(newcolumn != 0 && matrix[maxrow][newcolumn] < matrix[maxrow][newcolumn-1])
		return find2DPeak(matrix, rows, left, newcolumn);
	else
	{
		coord[0] = maxrow;
		coord[1] = newcolumn;
		return coord;
	}

}

/**
*	@param arr - matrix that will be searched
*	@param rows - the number of rows in the matrix
*	@param column - the column that we will find the maximum value in
*
*	@return the index of the row with the max value in that specific column
*/
int findGlobalMax(double arr[N][N], int rows, int column)
{
	int max = 0;
	int key = 0;

	for(int i = 0; i < rows; i++)
	{
		if(arr[i][column] > max)
		{
			key = i;
			max = arr[i][column];
		}
	}
	return key;
}

/**
*	neatly prints the array to console
*/
void printArray(double matrix[N][N])
{
	cout << "     ";
	for(int i = 0; i < N; i++)
		cout << setw(2) << i << " ";
	cout << endl << endl;

	for(int i = 0; i < N; i++)
	{
		cout << setw(2) << i << "   ";
		for(int j = 0; j < N; j++)
			cout << setw(2) <<  matrix[i][j] << " ";
		cout << endl;
	}
}

/**
*	Incase you don't want to manually fill up the matrix, you can call this function
*	and it will randomize all the indices for you, with numbers from 0 - max
*/
void randomizeMatrix(double matrix[N][N], int max)
{
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < N; j++)
			matrix[i][j] = rand()%max;
	}
}